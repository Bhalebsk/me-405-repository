# -*- coding: utf-8 -*-
## @file mcp9808.py
#  mcp9808.py acts as a module to read ambient temperatures
#
#  The user may run this script by first importing it, then using the following methods: check() celsius(), farenheit(). 
#  check() verifies the manufacturer ID for the mcp9808 breakoutboard. celsius() gets the temperature in celcius
#  and farenheit() uses the method celcius() then uses a conversion to output a temperature in farenheit.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package mcp9808.py
#  mcp9808.py acts as a module to read ambient temperatures
#
#  The user may run this script by first importing it, then using the following methods: check() celsius(), farenheit(). 
#  check() verifies the manufacturer ID for the mcp9808 breakoutboard. celsius() gets the temperature in celcius
#  and farenheit() uses the method celcius() then uses a conversion to output a temperature in farenheit.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
"""
Created on Tue Feb  9 16:41:58 2021

@author: Brand
"""

from pyb import I2C
import utime

#set up variables online fourms say this is not needed but I get an error that these variables dont exist if i dont set them equal to none here.
i2c_nucleo = None
data = None
channel = None


## A mcp9808 driver object
#
#  The mcp9808 class is used with a mcp9808 breakout board to collect ambient temperature data.
#  @author Brandon Halebsky
#  @copyright License Info
#  @date 2/10/21
class mcp9808:
    
    ## Constructor for mcp9808 driver
    #
    #  The primary purpose of the constructor is to set up an IC2 object, this object is then scanned 
    #  to find its channel which is used for I2C communication. This constructor also creates lots of 
    #  place holder variables.
    def __init__(self):
        #here is where the I2C object is created and then scanned for the correct channel
        self.i2c_nucleo = I2C(1, I2C.MASTER)
        self.channel = self.i2c_nucleo.scan()
        
        #below are place holders
        self.data = bytearray(2)
        self.bus_address = bytearray(2)
        self.binary_list_msb = [None,None,None,None,None,None,None,None]
        self.binary_list_lsb = [None,None,None,None,None,None,None,None]
        self.binary_counter = [128,64,32,16,8,4,2,1]
    
    ## Checks whether the manufacture ID on the mcp 9808 hardware matches with the datasheet
    #
    #  If the channel is not correct the user gets a print out of the channel they scanned    
    def check(self):
        
        #  This reads from the mcp980 breakout board's memory see page 16-17 of the data sheet 
        #  to see other registers. In this case we are going to register 6 or 0b0110.
        self.i2c_nucleo.mem_read(self.bus_address,self.channel[0],6)
        
        if self.bus_address[1] == 84:
            print('This is the correct manufacture ID')
        else:
            print('This is incorrect, your bus address was: ', self.bus_address[1])
            
    ## Checks the ambient temperature and outputs the temperature in degrees celsius
    #
    #  Data is read from the mcp9808 breakout board as 2 binary bytes. celsius()
    #  decodes the bytes using the registry on the mcp9808 datasheet. The temperature can
    #  be used by calling to the object's degreesC variable          
    def celsius(self):
        
        #read from register 5 using channel scanned in constructor
        self.i2c_nucleo.mem_read(self.data,self.channel[0],5)
        
        #assing most significant bit and least significant bit
        self.msb = self.data[0]
        self.lsb = self.data[1]

        # modify array of 1's and 0's to create a binary "number." ex. [1,0,1,1,0,0,0,0]
        for n in range(len(self.binary_list_msb)):
            if self.msb >=self.binary_counter[n]:
                self.binary_list_msb[n] = 1
                self.msb -= self.binary_counter[n]
            else:
                self.binary_list_msb[n] = 0
                
        for n in range(len(self.binary_list_lsb)):
            if self.lsb >=self.binary_counter[n]:
                self.binary_list_lsb[n] = 1
                self.lsb -= self.binary_counter[n]
            else:
                self.binary_list_lsb[n] = 0

        #here the binary "numbers," binary_list_msb and binary_list_lsb use the registry to convert those "bits" to  a number for temperature
        self.degreesC = self.binary_list_msb[4] * 2**7  + self.binary_list_msb[5] * 2**6  + self.binary_list_msb[6] * 2**5  + self.binary_list_msb[7] * 2**4  + self.binary_list_lsb[0] * 2**3  + self.binary_list_lsb[1] * 2**2  + self.binary_list_lsb[2] * 2**1 + self.binary_list_lsb[3] * 2**0  + self.binary_list_lsb[4] * 2**-1   + self.binary_list_lsb[5] * 2**-2  + self.binary_list_lsb[6] * 2**-3  + self.binary_list_lsb[7] * 2**-4
        if self.binary_list_msb[3] == 1:
            self.degreesC = -self.degreesC
        else:
            pass
        
        self.data = bytearray(2)   
        print(self.degreesC , 'degrees Celcius')
    ## Checks the ambient temperature and outputs the temperature in degrees farenheit
    #
    #  farenheit() uses celsius() to get a temperature in celsius then converts that temperature to farenheit.
    #  After calling farenheit(). The temperature can be used by calling to the object's degreesF variable              
    def farenheit(self):
        self.celsius()
        self.degreesF = (self.degreesC * 9/5) + 32
        print(self.degreesF)
            
        
        
#test code        
if __name__ == "__main__"       :
    y = mcp9808()
    y.celsius()
    utime.sleep(1)
        
'''        
for i in range(1):
    y = mcp9808()
    y.check()
    y.celsius()
    y.farenheit()'''
    
        
        

        


