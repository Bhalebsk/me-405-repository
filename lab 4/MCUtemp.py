# -*- coding: utf-8 -*-
## @file MCUtemp.py
#  MCUtemp.py acts as a module to read the temperature of a nucleo stm32L476 microcontroller core.
#
#  The user may run this script by first importing it, then using getTemp() to get a temperature reading of the core. 
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package MCUtemp.py
#  MCUtemp.py acts as a module to read the temperature of a nucleo stm32L476 microcontroller core.
#
#  The user may run this script by first importing it, then using getTemp() to get a temperature reading of the core. 
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
"""
Created on Tue Feb  9 21:47:12 2021

@author: Brand
"""

import pyb


## A MCUtemp driver object
#
#  The MCUtemp class is used with a nucleo stm32L476 to collect the core temperature.
#  @author Brandon Halebsky
#  @copyright License Info
#  @date 2/10/21
class MCUtemp:
    
    ## Constructor for MCU driver
    #
    #  The primary purpose of the constructor is to set up an ADCAll object and obtain the objects' v_ref to calibrate the sensor.
    def __init__(self):
        self.adcall = pyb.ADCAll(12,0x70000)
        self.adcall.read_vref()
        
    ## Checks the temperature of the core, truncates to 3 decimal places
    #
    # After getTemp() is called the temperature can be accesed by calling to the objects' tempC variable.
    def getTemp(self):
        self.tempC = self.adcall.read_core_temp()
        self.tempC = int(self.tempC*1000)/1000 #truncates to 3 decimals
        print(self.tempC)
'''        
for i in range(100):
    y = MCUtemp()
    y.getTemp()'''



