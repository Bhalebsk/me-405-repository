# -*- coding: utf-8 -*-
## @file lab0x04mainAppend.py
#  lab0x04mainAppend.py acts as a finite state machine to collect temperature data.
#
#  The user may run this script with mcp9808.py, MCUtemp.py, and microCSV.py in order to store the 
#  data collected as a .csv file. The temperature data is collected from the MCU on a nucleo STM32L476 
#  and the ambient temperature from a mcp9808 breakoutboard.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package lab0x04mainAppend.py
#  lab0x04mainAppend.py acts as a finite state machine to collect temperature data.
#
#  The user may run this script with mcp9808.py, MCUtemp.py, and microCSV.py in order to store the 
#  data collected as a .csv file. The temperature data is collected from the MCU on a nucleo STM32L476 
#  and the ambient temperature from a mcp9808 breakoutboard.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
"""
Created on Thu Feb  4 09:31:14 2021

@author: Brand
"""

import utime
from mcp9808 import mcp9808
from MCUtemp import MCUtemp
from microCSV import microCSVwriteLine

#variables are created
collectDataMCU = 0
collectDataAmbient = 0
time = 0 #minutes
end = 0
totaltime = 0
state = 0

try:
    while True:
        
        #State 0 initializes the loop by updating the current temperatures and creating a Start time
        if state == 0:
            MCU = MCUtemp()
            mcp = mcp9808()
            Start = utime.ticks_ms()
            state = 1
            
        #state 1 updates the temperature and time variable and then writes it to a .csv file every 60 seconds   
        elif state == 1:
            
            MCU.getTemp()
            mcp.celsius()
            collectDataMCU = MCU.tempC
            collectDataAmbient = mcp.degreesC
            
            microCSVwriteLine('testFileLab4a',collectDataMCU,collectDataAmbient,time)
            utime.sleep(60)
            
            time +=1
#if a keyboard interrupt is triggered calculate time running and print it out        
except KeyboardInterrupt:
    end = utime.ticks_ms()
    totaltime = utime.ticks_diff(end,Start)
    print(totaltime, 'ms ellapsed')
        


