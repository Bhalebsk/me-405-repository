# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 09:31:14 2021

@author: Brand
"""

import utime
from mcp9808 import mcp9808
from MCUtemp import MCUtemp
from microCSV import microCSVwrite

collectDataMCU = []
collectDataAmbient = []
time = [] #minutes
timeEllapsed = 0

end = 0
totaltime = 0
state = 0
try:
    while True:
        if state == 0:
            MCU = MCUtemp()
            mcp = mcp9808()
            Start = utime.ticks_ms()
            state = 1
        elif state == 1:
            time.append(timeEllapsed)
            MCU.getTemp()
            mcp.celsius()
            collectDataMCU.append(MCU.tempC)
            collectDataAmbient.append(mcp.degreesC)
            
            microCSVwrite('testFileLab4',len(collectDataMCU),collectDataMCU,collectDataAmbient,time)
            utime.sleep(60)
            timeEllapsed +=1
        
except KeyboardInterrupt:
    #microCSVwrite('testFileLab4',len(collectDataMCU),collectDataMCU,collectDataAmbient,time)
    end = utime.ticks_ms()
    totaltime = utime.ticks_diff(end,Start)
    print(totaltime)
        


