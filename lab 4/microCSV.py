# -*- coding: utf-8 -*-
## @file microCSV.py
#  microCSV.py acts as a data formatting tool which saves each data argument as its own column in a .csv file.
#
#  The user may run this script with lab0x04mainAppend.py in order to store the data collected as a .csv file.
#  This also can be used as a standard standalone .csv file creator for a pyb board using micropython.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package microCSV.py
#  microCSV.py acts as a data formatting tool which saves each data argument as its own column in a .csv file.
#
#  The user may run this script with lab0x04mainAppend.py in order to store the data collected as a .csv file.
#  This also can be used as a standard standalone .csv file creator for a pyb board using micropython. In order to
#  see the doxygen output for the functions please navigate to Files>File List>lab 4>microCSV.py
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
"""
Created on Thu Feb  4 08:48:15 2021

@author: Brand

"""


## Writes 3 lists into their respective columns in a .csv file
#
#  In order to use the microCSVwrite(fileName, Range, var1,var2,var3) method the arguments must be filled
#
#  @param fileName      A string that does not contain any file extension to be used as a file name.
#  @param Range         The number of times to iterate through the lists of data given as var1, var2, and var3
#  @param var1          A list of data to be formatted into a .csv file     
#  @param var2          A list of data to be formatted into a .csv file 
#  @param var3          A list of data to be formatted into a .csv file    
def microCSVwrite(fileName, Range,var1,var2,var3):
    #add file extension
    fileNameModified = fileName + '.csv'
    
    #open file in write mode
    with open(fileNameModified,'w') as output_file:
        
        #iterate through each position in each list
        for n in range(Range):
            string = str(var1[n])+','+str(var2[n])+','+str(var3[n])+'\n' # can add /r but this writes data to row 1,3,5, ...... , etc.
            output_file.write(string)
        output_file.close()
        
## Writes 3 variables at a time into their respective columns in a .csv file
#
#  In order to use the microCSVwrite(fileName, var1,var2,var3) method the arguments must be filled
#
#  @param fileName      A string that does not contain any file extension to be used as a file name.
#  @param var1          A float or an int to be formatted into a .csv file     
#  @param var2          A float or an int to be formatted into a .csv file
#  @param var3          A float or an int to be formatted into a .csv file            
def microCSVwriteLine(fileName,  var1,var2,var3):
    
    #add file extension
    fileNameModified = fileName + '.csv'
    
    #open file in append mode
    with open(fileNameModified,'a') as output_file:
        #for n in range(Range):
        string = str(var1)+','+str(var2)+','+str(var3)+'\n' # can add /r but this writes data to row 1,3,5, ...... , etc.
        output_file.write(string)
        output_file.close()

'''
#microCSVwrite('testCSV',9,x,z,y)
x = 0
y = 1
z = 5
for n in range(100):
    x += 10
    y = y*2
    z = z/(n+1)
    microCSVwriteLine('testCSVappend',x,z,y)'''
    
