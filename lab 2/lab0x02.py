# -*- coding: utf-8 -*-
## @file lab0x02.py
#  lab0x02.py acts as a reaction time test.
#
#  The user may run this script in order to test their reaction time.
#  This requires a Nucleo STM32L476. The green led light will come on and then the user presses the blue button as fast as they can.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/27/2021
#
#  @package lab0x02
#  lab0x02.py acts as a reaction time test.
#
#  The user may run this script in order to test their reaction time.
#  This requires a Nucleo STM32L476. The green led light will come on and then the user presses the blue button as fast as they can.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date January 27, 2021
"""
Created on Sat Jan 23 18:03:03 2021

@author: Brand
"""
import random
import utime
import pyb
import sys
import micropython
micropython.alloc_emergency_exception_buf(100) # suggested from micropython documentation

#define all required counters
x = 0
endTime = 0
TimePress= []
addingTime = 0

## Function to run after an interupt. The main purpose of this function is to save the start time at start of the interrupt. This works in conjunction with the button that triggers the intterupt to caputre the users reaction time.
#
#  callback() should be triggered by an interrupt. callback() will turn off the LED, save the time and set a trigger(x=1) when the blue button is pressed.

def callback(_):
    '''
    @brief Function to run after an interupt.
    
    @details    The main purpose of this function is to save the start time at start of the interrupt. This works in conjunction with the button that triggers the intterupt to caputre the users reaction time.
                callback() should be triggered by an interrupt. callback() will turn off the LED, save the time and set a trigger(x=1) when the blue button is pressed.

    '''
    global x,endTime
    endTime = timePassed  
    blinko.low()
    x=1
    
#set up button for interrupt
extintButton = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP,  callback)

#led set up
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)


#timer set up. this creates a timer which reaches its max value once every 1ms.
Rtimer = pyb.Timer(5,prescaler = 39, period =1999 )

#this sets up an preallocates a lot of memery for the counter which increases onc every ms.
timePassed = 0x7FFFFFFF
timePassed = 0


## Function to be used with a timer interupt to create a counter. In this case the counter increase by one every ms.
#
#  @param which_timer This parameter was not used during this lab. It is currently a reminder to myself that it may be useful later in the quarter.

def timInt (which_timer):
    '''
    @brief   Function to be used with a timer interupt to create a counter. In this case the counter increase by one every ms.
    
    @param which_timer This parameter was not used during this lab. It is currently a reminder to myself that it may be useful later in the quarter.
    '''
    global timePassed
    timePassed +=1   # with timer 5, prescaler = 39, period =1999 on nucleo32L476

#This is where the timer, Rtimer, is told to use the callback function timInt
Rtimer.callback(timInt)


try:
    while True:
        #this generates a random number from 2000 to 3000
        randNumb = random.randrange(2000,3000)
        
        #this uses the random number generated above and sleeps for that many ms.
        utime.sleep_ms(randNumb)
        
        #create start time variable
        startTime = timePassed
        
        #turn on LED
        blinko.high()
        
        #This is for the case where the user does not react within one second. The code will progress after the 1 second has passed.
        #In the case where a user presses the blue button an interupt is triggered mid sleep.
        utime.sleep_ms(1000)
        
        
        
        #if interupt is triggered by pressing blue button
        if x==1:
            
            #This prevents the user from pressing the button at the wrong time causing a negative number to be saved.
            if endTime-startTime >= 0:
                #save next reaction time
                TimePress.append(endTime-startTime)
                
                #add next reaction time to total time variable
                addingTime += (endTime-startTime)
                
                '''#print(TimePress, 'ms') # for myself during testing'''
                
                #reset flag
                x=0
                
            #this prints out an error message, turns the led off and sets the trigger back to 0    
            else:
                print('Please try again, press the blue button when the green LED turns on')
                blinko.low()
                x = 0
        #this is when interrupt is NOT triggered
        elif x == 0:
            blinko.low()
            continue

#This handles the final steps to close out the program when a user presses ctrl+c
except KeyboardInterrupt:
    avgTime = addingTime/len(TimePress)
    
    
    print('Your average button press time was,', avgTime, 'ms')
    Rtimer.deinit()
    
    sys.exit(0)
 
    
