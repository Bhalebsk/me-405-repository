# -*- coding: utf-8 -*-
#@file lab0x01old.py
"""
Created on Thu Jan 14 08:31:34 2021

@author: Brand
"""
import keyboard

pushed_key = None
cent = 0
def on_keypress (thing):
    """ Callback which runs when the user presses a key. This function was given to the class by the instructor.
    """
    global pushed_key

    pushed_key = thing.name


if __name__ == "__main__":

    dimes = 0                # Pursue a career in accounting

    keyboard.on_press (on_keypress)  ########## Set callback

    # Run a simple loop which responds to some keys and ignores others.
    # If someone presses control-C, exit this program cleanly
    while True:
        try:
            # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == "0" or pushed_key == "c":
                    cent +=1
                    print ("This is worth a cent", cent)

                elif pushed_key == '1':
                    print ("Non-wooden nickel detected")

                elif pushed_key == '2':
                    dimes += 1
                    print ("We're up to our {:d}th dime".format (dimes))

                elif pushed_key == 'e':
                    print ("I've seen an 'E'")

                pushed_key = None

        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
        except KeyboardInterrupt:
            break

    print ("Control-C has been pressed, so it's time to exit.")
    keyboard.unhook_all ()



