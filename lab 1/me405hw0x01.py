# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 09:46:51 2021

@author: Brand
"""
import lab0x01shares as share


def getChange(price,payment):
    
    if payment >= price:
        
        #calculate delta
        delta = payment - price
        
        #print(delta)
        
        #calculate twenties
        twentiesHold = round((delta % 20),2)
        #print(twentiesHold)
        moneyInTwenties = round(delta - twentiesHold,2)
        #print(moneyInTwenties)
        updateDelta = twentiesHold
        numberOfTwenties = int(moneyInTwenties/20)
        #print(updateDelta)
        
        #calculate tens
        tensHold = round((updateDelta % 10),2)
        moneyInTens = round(updateDelta - tensHold,2)
        updateDelta = tensHold
        numberOfTens = int(moneyInTens/10)
        #print(updateDelta)
        
        #calculate fives
        fivesHold =round((updateDelta % 5),2)
        moneyInFives = round(updateDelta - fivesHold,2)
        updateDelta = fivesHold
        numberOfFives = int(moneyInFives/5)
        #print(updateDelta)
        
        #calculate ones
        onesHold = round((updateDelta % 1),2)
        moneyInOnes = round(updateDelta - onesHold,2)
        updateDelta = onesHold
        numberOfOnes = int(moneyInOnes/1)
        #print(updateDelta)
        
        #calculate quarters
        quartersHold = round((updateDelta % .25),2)
        moneyInQuarters = round(updateDelta - quartersHold,2)
        #print(moneyInQuarters)
        updateDelta = quartersHold
        numberOfQuarters = int(moneyInQuarters/.25)
        #print(updateDelta)
        
        #calculate dimes
        dimesHold = round((updateDelta % .1),2)
        #print(dimesHold)
        moneyInDimes = round(updateDelta - dimesHold,2)
        #print(moneyInDimes)
        updateDelta = dimesHold
        numberOfDimes = int(moneyInDimes/.1)
        #print(updateDelta)
        
        #calculate nickels
        nickelsHold = round((updateDelta % .05),2)
        moneyInNickels = round(updateDelta - nickelsHold,2)
        #print(moneyInNickels)
        updateDelta = nickelsHold
        numberOfNickels = int(moneyInNickels/.05)
        #print(updateDelta)
        
        #calculate pennies
        penniesHold = round((updateDelta % .01),2)
        moneyInPennies = round(updateDelta - penniesHold,2)
        #print(moneyInPennies)
        updateDelta = penniesHold
        numberOfPennies = int(moneyInPennies/.01)
        #print(updateDelta)
        
        #payment tuple
        share.payment = (numberOfPennies, numberOfNickels, numberOfDimes, numberOfQuarters, numberOfOnes, numberOfFives, numberOfTens, numberOfTwenties)
        #print(payment)
        #return payment
        #print('The correct change is, ', share.payment[7], ' twenty dollar bill(s), ', share.payment[6], ' ten dollar bill(s), ', share.payment[5], ' five dollar bill(s), ', share.payment[4], ' one dollar bill(s), ', share.payment[3], ' quarter(s), ', share.payment[2], ' dime(s), ', share.payment[1], ' nickel(s), and ', share.payment[0], ' pennies.')
    else:
        print('You did not pay enough. Try again!')     
    pass

if __name__ == "__main__":
    
    for n in range(1):
        getChange(19.01,40.00)
        getChange(12.34,777.77)
