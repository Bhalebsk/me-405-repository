# -*- coding: utf-8 -*-
## @file lab0x01.py
#  Lab0x01.py simulates a vending machine.
#
#  The user may press a number from 0-7 to add "money" to the vending machine.
#  Then the user my press p,d,s, or c, to select a "drink." The user may also press, e, to eject their "money."
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/20/2021
#
#  @package lab0x01
#  Lab0x01.py simulates a vending machine.
#
#  The user may press a number from 0-7 to add "money" to the vending machine.
#  Then the user my press p,d,s, or c, to select a "drink." The user may also press, e, to eject their "money."
#
#  @author Your Name
#
#  @copyright License Info
#
#  @date January 1, 1970
"""
Created on Sat Jan 16 14:12:48 2021

@author: Brand
"""

import keyboard


#define money counters
pennies = 0
nickels = 0
dimes = 0
quarters = 0
ones = 0
fives = 0
tens = 0
twenties = 0

#Total_money reset upon restarting code
Total_money = 0

#initialize triggers
penny = False
nickel = False
dime = False
quarter = False
one = False
five = False
ten = False
twenty = False
coke = False
pepsi = False
sprite = False
pepper = False

pushed_key = None
def on_keypress (thing):
    """ Callback which runs when the user presses a key. This function was given to the class by the instructor.
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press(on_keypress)


## Function to calculate the change given in an easy to read format.
#
#  getChange() can be used in the following format, getChange(price, InitPayment)
#
#  @param price The price of the item
#  @param InitPayment The payment which the price will be deducted from
def getChange(price,InitPayment):
    global payment
    
    #when the payment is valid
    if InitPayment >= price:
        
        #calculate delta
        delta = InitPayment - price
        
        #calculate twenties
        twentiesHold = round((delta % 20),2)
        moneyInTwenties = round(delta - twentiesHold,2)
        updateDelta = twentiesHold
        numberOfTwenties = int(moneyInTwenties/20)
        
        #calculate tens
        tensHold = round((updateDelta % 10),2)
        moneyInTens = round(updateDelta - tensHold,2)
        updateDelta = tensHold
        numberOfTens = int(moneyInTens/10)
        
        #calculate fives
        fivesHold =round((updateDelta % 5),2)
        moneyInFives = round(updateDelta - fivesHold,2)
        updateDelta = fivesHold
        numberOfFives = int(moneyInFives/5)
        
        #calculate ones
        onesHold = round((updateDelta % 1),2)
        moneyInOnes = round(updateDelta - onesHold,2)
        updateDelta = onesHold
        numberOfOnes = int(moneyInOnes/1)
        
        #calculate quarters
        quartersHold = round((updateDelta % .25),2)
        moneyInQuarters = round(updateDelta - quartersHold,2)
        updateDelta = quartersHold
        numberOfQuarters = int(moneyInQuarters/.25)
        
        #calculate dimes
        dimesHold = round((updateDelta % .1),2)
        moneyInDimes = round(updateDelta - dimesHold,2)
        updateDelta = dimesHold
        numberOfDimes = int(moneyInDimes/.1)
        
        #calculate nickels
        nickelsHold = round((updateDelta % .05),2)
        moneyInNickels = round(updateDelta - nickelsHold,2)
        updateDelta = nickelsHold
        numberOfNickels = int(moneyInNickels/.05)
        
        #calculate pennies
        penniesHold = round((updateDelta % .01),2)
        moneyInPennies = round(updateDelta - penniesHold,2)
        updateDelta = penniesHold
        numberOfPennies = int(moneyInPennies/.01)
        
        #payment tuple
        payment = (numberOfPennies, numberOfNickels, numberOfDimes, numberOfQuarters, numberOfOnes, numberOfFives, numberOfTens, numberOfTwenties)
        
    else:
        print('You did not pay enough. Try again!')     
    pass

#set triggers for FSM
x = 1
state = 0

#run forever
while True:
    
    #start-up
    if state == 0:
        
        #this if statement triggers once on original startup, does not trigger when user presses e.
        if x ==1:
            
            print('Vending Machine - Initializing')
        print(' ')
        print('Welcome, please insert the required ammount for your prefered beverage.')
        print('Press "P" for Popsi, "C" for Cuke, "S" for Spryte, or "D" for Dr. Pupper')
        print('Press "0" to add a penny, "1" to add a nickel, "2" to add a dime, "3" to add a quarter, "4" to add $1, "5" to add $5, "6" to add $10, "7" to add $20')
        x = 0
        state = 1
    
        #wait for user input state
    elif state == 1:
        if pushed_key:
            
            #drink selection
            if pushed_key =="C" or pushed_key == "c": #cuke
                coke = True
                state = 3
            elif pushed_key =="P" or pushed_key == "p": #popsi
                pepsi = True
                state = 3
            elif pushed_key =="S" or pushed_key == "s": #spryte
                sprite = True
                state = 3
            elif pushed_key =="D" or pushed_key == "d": #dr. Pupper
                pepper = True
                state = 3
                
            #Money input    
            elif pushed_key == "0":  #pennies
                penny = True
                state = 2
            elif pushed_key == "1":  #nickels
                nickel = True
                state = 2
            elif pushed_key == "2":  #dimes
                dime = True
                state = 2
            elif pushed_key == "3":  #quarters
                quarter = True
                state = 2
            elif pushed_key == "4":  #ones
                one = True
                state = 2
            elif pushed_key == "5":  #fives
                five = True
                state = 2
            elif pushed_key == "6":  #tens
                ten = True
                state = 2
            elif pushed_key == "7":  #twenties
                twenty = True
                state = 2
            
            #Eject
            elif pushed_key =="E" or pushed_key == "e":
                state = 4
            else:
                pass
            #this stores a set of either true or false in order to 
            #check if keypress needs to be reset
            logic_tuple = [penny,nickel,dime,quarter,one,five,ten,twenty,coke,pepsi,pepper,sprite]
        else:
            pass
        

    #Reading Change state        
    elif state == 2:
        #This is where the set of data is checked to see if keypress should be reset
        for i in range(len(logic_tuple)):
            if logic_tuple[i] ==True:
                pushed_key = None
                
            else:
                pass
        if penny == True:
            pennies += 1
            penny = False
        elif nickel == True:
            nickels += 1
            nickel = False
        elif dime == True:
            dimes += 1
            dime = False
        elif quarter == True:
            quarters += 1
            quarter = False
        elif one == True:
            ones += 1
            one = False
        elif five == True:
            fives += 1
            five = False
        elif ten == True:
            tens += 1
            ten = False
        elif twenty == True:
            twenties += 1
            twenty = False
        else:
            print('How did you get here??')
        Total_money = ((1*pennies)+(5*nickels)+(10*dimes)+(25*quarters)+(100*ones)+(500*fives)+(1000*tens)+(2000*twenties))/100
        print('Remaining Balance: $', Total_money)
        state = 1
    
    #Dispense drink state    
    elif state == 3:
        for i in range(len(logic_tuple)):
            if logic_tuple[i] ==True:
                pushed_key = None
                
            else:
                pass
        
        #determine price of drink
        if coke == True:
            money_req = 100/100
            coke = False
        elif pepsi == True:
            money_req = 120/100
            pepsi = False
        elif sprite == True:
            money_req = 85/100
            sprite = False
        elif pepper == True:
            money_req = 110/100
            pepper = False
        else:
            pass
        
        #choose what to do
        if Total_money < money_req:
            print('Please insert more money and try again')
            state = 1
        if Total_money >= money_req:
                       
            getChange(money_req,Total_money)
            
            pennies = payment[0]
            nickels = payment[1]
            dimes = payment[2]
            quarters = payment[3]
            ones = payment[4]
            fives = payment[5]
            tens = payment[6]
            twenties = payment[7]
            
            Total_money = ((1*pennies)+(5*nickels)+(10*dimes)+(25*quarters)+(100*ones)+(500*fives)+(1000*tens)+(2000*twenties))/100
            
            if money_req ==100/100:
                print('Thank you for purchasing a Cuke.')
            elif money_req == 120/100:
                print('Thank you for purchasing a Popsi.')
            elif money_req == 85/100:
                print('Thank you for purchasing a Spryte.')
            elif money_req == 110/100:
                print('Thank you for purchasing a Dr. Pupper.')
                
            money_req = 0        
            print('Remaining Balance: $', Total_money)
            state = 1
    
    #eject change state    
    elif state == 4:
        pushed_key = 0
        ejectChange = (pennies,nickels,dimes,quarters,ones,fives,tens,twenties)
        print('Your change is, ', ejectChange[7], ' twenty dollar bill(s), ', ejectChange[6], ' ten dollar bill(s), ', ejectChange[5], ' five dollar bill(s), ', ejectChange[4], ' one dollar bill(s), ', ejectChange[3], ' quarter(s), ', ejectChange[2], ' dime(s), ', ejectChange[1], ' nickel(s), and ', ejectChange[0], ' pennies.')
        pennies = 0
        nickels = 0
        dimes = 0
        quarters = 0
        ones = 0
        fives = 0
        tens = 0
        twenties = 0
        Total_money = 0
        state = 0