var namespaces_dup =
[
    [ "BalanceMain", null, [
      [ "py", "namespaceBalanceMain_1_1py.html", null ]
    ] ],
    [ "lab0x01", "namespacelab0x01.html", null ],
    [ "lab0x02", "namespacelab0x02.html", null ],
    [ "lab0x03main", null, [
      [ "py", "namespacelab0x03main_1_1py.html", null ]
    ] ],
    [ "lab0x03PCend", null, [
      [ "py", "namespacelab0x03PCend_1_1py.html", null ]
    ] ],
    [ "lab0x04mainAppend", null, [
      [ "py", "namespacelab0x04mainAppend_1_1py.html", null ]
    ] ],
    [ "mcp9808", null, [
      [ "py", "namespacemcp9808_1_1py.html", null ]
    ] ],
    [ "MCUtemp", null, [
      [ "py", "namespaceMCUtemp_1_1py.html", null ]
    ] ],
    [ "microCSV", null, [
      [ "py", "namespacemicroCSV_1_1py.html", null ]
    ] ],
    [ "MotorDriver", null, [
      [ "py", "namespaceMotorDriver_1_1py.html", null ]
    ] ],
    [ "MotorEncoder", null, [
      [ "py", "namespaceMotorEncoder_1_1py.html", null ]
    ] ],
    [ "Rtouch", null, [
      [ "py", "namespaceRtouch_1_1py.html", null ]
    ] ]
];