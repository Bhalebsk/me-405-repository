var lab0x03PCend_8py =
[
    [ "on_keypress", "lab0x03PCend_8py.html#a9f0227667bfb95794d1306e393b438e0", null ],
    [ "sendChar", "lab0x03PCend_8py.html#af32f435ec5eb3efdd4515fa06cab5952", null ],
    [ "checkVal", "lab0x03PCend_8py.html#ad7ca8c8c76d4142f88399265bc35df54", null ],
    [ "EndFound", "lab0x03PCend_8py.html#a48deb93ca23e4f043ef6dec8b361da09", null ],
    [ "EndPosition", "lab0x03PCend_8py.html#af5adc1a4fe5592188fa93dfc1ff2cd1b", null ],
    [ "EndTrigger", "lab0x03PCend_8py.html#ae464826e30185b68c586c10b0a80af25", null ],
    [ "k", "lab0x03PCend_8py.html#a8d6f9dd3fd65334a20da42408d89ed79", null ],
    [ "myval", "lab0x03PCend_8py.html#a7bd788acd06c05ed188320f9e20f4cf1", null ],
    [ "myvalList", "lab0x03PCend_8py.html#a1e6f390418528cdb71a1f6700b7df6bb", null ],
    [ "NewTimeUS", "lab0x03PCend_8py.html#aff60f77cf84dd3fb74edfcda84e127a6", null ],
    [ "NewVoltageList", "lab0x03PCend_8py.html#adc947875e028a267d80ac5af3e90bdff", null ],
    [ "pushed_key", "lab0x03PCend_8py.html#a9501f13303f27b972d1f6632e14c59d6", null ],
    [ "serialPC", "lab0x03PCend_8py.html#a4979e30156bfb2293d6b48b028f00eb2", null ],
    [ "startGraph", "lab0x03PCend_8py.html#a78e5e5ec30f1c820640d285f74d11fb6", null ],
    [ "StartPosition", "lab0x03PCend_8py.html#a39faca03ed04df69ab1eca8edeed9abf", null ],
    [ "StartTrigger", "lab0x03PCend_8py.html#a60a20705f5a12adb69abcf9bfa8d9845", null ],
    [ "TimeStepResponse", "lab0x03PCend_8py.html#ae7ff448bbbe69b55aeab0f0333c86e23", null ],
    [ "TimeUS", "lab0x03PCend_8py.html#a5749accd9b727afade48b692d1541953", null ],
    [ "TotalTime", "lab0x03PCend_8py.html#adb7c5893a719099ae7c5dbdb93e8ee3a", null ],
    [ "VoltageList", "lab0x03PCend_8py.html#a4f8053d5009cb5c50912eb2ca8e01b9c", null ],
    [ "writer", "lab0x03PCend_8py.html#a5fb06a9dc9865dca9c6a310e914a18f2", null ]
];