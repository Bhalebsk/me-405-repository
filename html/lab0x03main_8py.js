var lab0x03main_8py =
[
    [ "callback", "lab0x03main_8py.html#a69efc4acaa0448d370d8aeb454b89c31", null ],
    [ "BigList", "lab0x03main_8py.html#abfc01e4409ff9dab9a6897fcf67b107d", null ],
    [ "blinko", "lab0x03main_8py.html#a8d322a82a45fe614164b3ad59257cf9f", null ],
    [ "ButtonVoltage", "lab0x03main_8py.html#aa1a8dc541af7b9e1d0d744d45b2d1703", null ],
    [ "DataBuffer", "lab0x03main_8py.html#a99a3c929944accf0882f97cca6d5a696", null ],
    [ "DataBufferSize", "lab0x03main_8py.html#ae3299a54458d7d434963d2797c3b2d12", null ],
    [ "dataCounter", "lab0x03main_8py.html#a34a5256cdb091773df1d8c45293de25c", null ],
    [ "endTime", "lab0x03main_8py.html#a8c101b5067f1166676c72af97f58154c", null ],
    [ "extintButton", "lab0x03main_8py.html#acdc7aa19a0cf83da3c9d5748b5ef15d8", null ],
    [ "pinA0", "lab0x03main_8py.html#a2409ef1b77670d3d8c869293cb677507", null ],
    [ "serComm", "lab0x03main_8py.html#aa0c6bbf545a87c90c9c6f909a66f38fe", null ],
    [ "startTime", "lab0x03main_8py.html#aca603c87e88eda26c9dff12734dd9c31", null ],
    [ "timeDiff", "lab0x03main_8py.html#a2e0401a52aecd43a55b0f790b2facc83", null ],
    [ "val", "lab0x03main_8py.html#ad20b4b864cfeaeecceed7a6ff8b33ff1", null ],
    [ "x", "lab0x03main_8py.html#a5dec5d2f82589c81ce712a875198abb5", null ]
];