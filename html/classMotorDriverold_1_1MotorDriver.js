var classMotorDriverold_1_1MotorDriver =
[
    [ "__init__", "classMotorDriverold_1_1MotorDriver.html#a4efd787754031e1d7f092f2ed2e8f2e6", null ],
    [ "disable", "classMotorDriverold_1_1MotorDriver.html#aea3b9d53eead53997d36d5be0023227d", null ],
    [ "enable", "classMotorDriverold_1_1MotorDriver.html#af36e42645ad38cd92951c38ee0f07b6f", null ],
    [ "set_duty", "classMotorDriverold_1_1MotorDriver.html#acd91be8f67f82d017b01938b194ed845", null ],
    [ "pinA15", "classMotorDriverold_1_1MotorDriver.html#a98cf8aca1d29fccfb8c30c7c29f5ad24", null ],
    [ "pinB4", "classMotorDriverold_1_1MotorDriver.html#a9c9d0c20f93649abe9df0538771a4310", null ],
    [ "pinB5", "classMotorDriverold_1_1MotorDriver.html#a3f150290da8e4753c89384fd447cb2e8", null ],
    [ "t3ch1", "classMotorDriverold_1_1MotorDriver.html#a68225077a0faec228e25f5a099c69134", null ],
    [ "t3ch2", "classMotorDriverold_1_1MotorDriver.html#a540c3b44b401a0c7de7f65ca3f31b68e", null ],
    [ "tim3", "classMotorDriverold_1_1MotorDriver.html#a1e44666a20e53b72de52cf5123937089", null ]
];