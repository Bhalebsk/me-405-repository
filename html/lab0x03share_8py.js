var lab0x03share_8py =
[
    [ "csv_omega", "lab0x03share_8py.html#a9beef128f8d57dd331d0ea0a39868fa6", null ],
    [ "csv_position", "lab0x03share_8py.html#a40dec9a3891b76ae57c188b4bd220b53", null ],
    [ "csv_trigger", "lab0x03share_8py.html#a12a27e4b01ce7e18e686cadb1ca9dc27", null ],
    [ "duty", "lab0x03share_8py.html#a5c3fc7f4dfddb5fe02d999b6ac5fa06a", null ],
    [ "integral_err", "lab0x03share_8py.html#a88edb1e3989c175e03da1f0b87b179da", null ],
    [ "Motor_state", "lab0x03share_8py.html#a66c5776745bbd51a83c05478eb87a78a", null ],
    [ "omega_real", "lab0x03share_8py.html#aa0a7ff64367a1e8f97996a699e61a348", null ],
    [ "position", "lab0x03share_8py.html#a60ef3f13fa8487a8708cec8b58ff9167", null ],
    [ "position2", "lab0x03share_8py.html#ac23eb5e8bb462872ad31a586003acddc", null ],
    [ "prev_duty", "lab0x03share_8py.html#a77fafdd2c916c7cdb31baf4ff5040821", null ],
    [ "prev_position", "lab0x03share_8py.html#a94d46f79e68972fbfe88f73653dcbeef", null ],
    [ "prev_position2", "lab0x03share_8py.html#a987212e7b352dff43486d01d35cebcf9", null ],
    [ "raw_delta", "lab0x03share_8py.html#a81b393230c6ecab94f73904279e0eda3", null ],
    [ "raw_delta2", "lab0x03share_8py.html#a7be480aaded608119245efea98f0b71d", null ],
    [ "raw_pos", "lab0x03share_8py.html#a6eb3d43ba7f68a00a942faa989a0283b", null ],
    [ "raw_pos2", "lab0x03share_8py.html#a878e6158c57fb4609e3b5bf952df3ec9", null ],
    [ "real_delta", "lab0x03share_8py.html#a6f742520b2d4412856fae7b54b94aa78", null ],
    [ "rolls", "lab0x03share_8py.html#a3cf74dc94b1655e264449304407e5932", null ],
    [ "rolls2", "lab0x03share_8py.html#ae807ed14b65f7a0274378a2cad92d9d7", null ],
    [ "tim", "lab0x03share_8py.html#a780b8d5f208858102d6228aba4b9966a", null ],
    [ "tim2", "lab0x03share_8py.html#a8f10e45d81a09371c1356d9c1ad644f7", null ]
];