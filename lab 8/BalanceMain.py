# -*- coding: utf-8 -*-
## @file BalanceMain.py
#  BalanceMain.py acts as a controller for a Balancing platform and a ball. In order to use BalanceMain.py you must start the code when the platform is Level.
#
#  BalanceMain.py integrates a Motor Driver, Motor Encoder, and a Resistiver Touch panel in order to keep a ball balanced on a rotating platform.
#  These all work together to create a control system that uses four gains to modify the values, [x_dot, theta_dot, x, theta]. This is to say
#  that the as each of these values gets close to zero the motors will try to compensate less. This leads to the platform staying flat.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 3/17/2021
#
#  @package BalanceMain.py
#  BalanceMain.py acts as a controller for a Balancing platform and a ball. In order to use BalanceMain.py you must start the code when the platform is Level.
#
#  BalanceMain.py integrates a Motor Driver, Motor Encoder, and a Resistiver Touch panel in order to keep a ball balanced on a rotating platform.
#  These all work together to create a control system that uses four gains to modify the values, [x_dot, theta_dot, x, theta]. This is to say
#  that the as each of these values gets close to zero the motors will try to compensate less. This leads to the platform staying flat.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 3/17/2021
"""
Created on Wed Mar 17 17:26:02 2021

@author: Brand
"""

import MotorEncoder
import MotorDriver
import utime
import lab0x03share
import Rtouch


#here I calculated the duty constant
R = 2.21  #[ohms]
Vdc = 12  #[V]
Kt = 13.8/1000  #[Nm/A]

K_duty = 3*R/(Vdc*Kt)   #the 3 in there is a constant to allow the motors to work in our system.
                        #I have found that the motors will not rotate unless the duty cycle is set to above abs(50)


#Here the motor Encoders are set up
Encoder1 = MotorEncoder.MotorEncoder(num=1)
Encoder2 = MotorEncoder.MotorEncoder(num=2)


#Here the pins for the Motors are defined
pin_nSLEEP  = 'A15';
pin_nFAULT  = 'B2';
pin_IN1     = 'B0';
pin_IN2     = 'B1';


pin_IN1_2     = 'B4';
pin_IN2_2     = 'B5';

#Here the time channel for the motors is selected
tim     = 3;

#Here the Motors are set up
Motor1 = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1_2, pin_IN2_2, tim)
#print(Motor1.pinA15.value(),'-1-')
Motor2 = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, tim)
#print(Motor1.pinA15.value(),'-2-')


#Here initial variables are defined to 'jump start' the while True loop
theta_y = 0
theta_x = 0
x = 0
y = 0
TouchData = None


#Here the constants are defined for the control system. Each of these will be multiplied by thier respective variable to produce a motor torque value to feed into the motor driver.
K1 = 1 
K2 = .07
K3 = 1.5
K4 = .9
#print(Motor1.pinA15.value(),'-3-')

#Here the external interupt is diabled while the motor is enabled and then the external interrupt is enabled again
Motor1.pinB2.disable()
#print(Motor1.pinA15.value(),'-4-')
Motor1.enable()
#print(Motor1.pinA15.value(),'-5-')
Motor1.pinB2.enable()
#print(Motor1.pinA15.value(),'-6-')

#Here the touch panel is set up
Touch = Rtouch.Rtouch('A7','A1','A6','A0',.179,.101,2000,1880)


time1 = utime.ticks_us()
while True:
    
    #The encoders update each loop
    Encoder1.run()
    Encoder2.run2()
    
    #here the total time of the lopp is calculated. This will be inaccurate for the first loop run.
    period = utime.ticks_diff(utime.ticks_us(),time1)
    time1 = utime.ticks_us()
    
    #Here the angle and angular velocity of the motor is found, since the angle is centered around zero for both the motor and platform angles no conversion is needed to convert the motor angle to platform angle.
    theta_y_old = theta_y  #motor 1
    theta_x_old =theta_x  # motor 2
    
    theta_y = lab0x03share.position*(360/4000)
    theta_x = lab0x03share.position2*(360/4000)
    
    theta_dot_y = (theta_y - theta_y_old)/period
    theta_dot_x = (theta_x - theta_x_old)/period
    
    #here the touchpanel data is saved to a variable for use in determining where the ball is positioned on the platform.
    TouchData = Touch.readAll()
    print(TouchData)
    
    
    #This If statement determines whethere there is contact on the platform
    if TouchData[2] == True:
        #if there is contact with the platform save the x and y values and use the x and y values to calculate x_dot and y_dot.
        x_old = x
        y_old = y
        
        x = TouchData[0]
        y = TouchData[1]
        
        x_dot = (x-x_old)/period
        y_dot = (y-y_old)/period
        
    elif TouchData[2] == False:
        #if not ignore all the random information and set x,y,x_dot and y_dot equal to zero to prevent random movements.
        x = 0
        y = 0
        x_dot = 0
        y_dot = 0
    
    #These equations are what determines the torque the motor needs to balance the ball.
    #each variable is multiplied by its respective gain constant. It is useful to think of these as a 'weight of importance' for each variable.
    #That is to say if you want the system to respond more to a change in velocity of the ball you would increase K1, x_dot's respective gain.
    Torque1 = -K1*y_dot-K2*theta_dot_y -K3*y - K4*theta_y
    Torque2 = -K1*x_dot -K2*theta_dot_x -K3*x - K4*theta_x
    
    
    #Here each duty cycle is multiplied by the motor gain and converted to an integer.
    #This also prevents a number greater than 100 or lower than -100 from being sent to the motordriver
    Duty_cycle1 = int(K_duty*Torque1)
    if Duty_cycle1 > 100:
        Duty_cycle1 = 100
    elif Duty_cycle1 < -100:
        Duty_cycle1 = -100
        
    Duty_cycle2 = int(K_duty*Torque2)
    if Duty_cycle2 > 100:
        Duty_cycle2 = 100
    elif Duty_cycle2 < -100:
        Duty_cycle2 = -100
        
        
    #print(Duty_cycle1,theta_y,theta_dot_y, 'Duty Cycle, thetay , theta_doty', Duty_cycle2,theta_x,theta_dot_x )
    #^this is for debugging
    
    #Here each motor is set to its respective duty cycle
    Motor1.set_duty(Duty_cycle1)
    Motor2.set_duty(Duty_cycle2)
    
    
    


