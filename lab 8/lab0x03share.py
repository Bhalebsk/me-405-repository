# -*- coding: utf-8 -*-

##@file lab0x03share.py

"""
##@file lab0x03share.py

ME305 This file serves as a workaround to global variables. The variables which are needed
in both the UI, ClosedLoop and MotorEncoder are stored here.

ME405 This file will bring together MotorEncoder.py, MotorDriver.py in order to avoid using global variables.

@author: Brand
"""
# MotorEncoder
position = None
raw_delta = None
tim = None
rolls = None
raw_pos = None

#MotorEncoder 2
position2 = None
raw_delta2 = None
tim2 = None
rolls2 = None
raw_pos2 = None

#ClosedLoop
duty = 0
prev_position = None
real_delta = 0
prev_duty = 0
integral_err = 0
omega_real = 0

#closedloop2
prev_position2 = None
real_delta = 0


#data processing lab 7
csv_omega = 0
csv_trigger = 0
csv_position = 0

#MotorDriverShare
Motor_state = None   