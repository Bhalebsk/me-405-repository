# -*- coding: utf-8 -*-
## @file MotorDriver.py
#  MotorDriver.py acts as a driver for a motor.
#
#  A MotorDriver object can enable() the motor by setting pinA15 high, disable() the motor by setting pinA15 low, set the duty cycle using set_duty(percent of power), 
#  and includes a fault detection method which triggers an intterupt to stop the motors.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package MotorDriver.py
#  MotorDriver.py acts as a driver for a motor.
#
#  A MotorDriver object can enable() the motor by setting pinA15 high, disable() the motor by setting pinA15 low, set the duty cycle using set_duty(percent of power), 
#  and includes a fault detection method which triggers an intterupt to stop the motors.
#
#  @author Brandon Halebsky, Logan Garby, Rebecca Rodriguez
#
#  @copyright License Info
#
#  @date 3/10/2021




import pyb
import micropython
 
import lab0x03share
micropython.alloc_emergency_exception_buf(150)




class MotorDriver:
    ''' 
    @brief  This class implements a motor driver for the ME405 board. 
    
    @details The MotorDriver class is initalized with the motor's sleep, fault, and input pins
    as well as their corresponding timers and timer channels. The class contains 
    methods to enable and disable the motor, set the duty cycle, determine if there
    is a fault, and reset the fault pin.
    '''
    
    def __init__ (self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety. Balancing 
        board motors pins correspond to the following class inputs:
        
            * 'A15' for nSLEEP_pin
            * 'B2' for nFAULT_pin
            * 'B4' for Motor 1 IN1_pin
            * 'B5' for Motor 1 IN2_pin
            * 'B0' for Motor 2 IN1_pin
            * 'B1' for Motor 2 IN2_pin
            
        Additionally, timer 3 is used for all motor input pins with timer channels
        1 and 2 corresponding to pins B4 and B5 respectively and timer channels 
        3 and 4 corresponding to pins B0 and B1 respectively.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param nFAULT_pin   A pyb.Pin object to use as the fault pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
                            IN1_pin and IN2_pin. '''
                            
                            
        #This sets up the correct channels for the motor to run                    
        print ('Creating a motor driver - Motor Driver Class')
        
        ## nSLEEP pin object (set to high to enable the motors)
        self.pinA15 = pyb.Pin(nSLEEP_pin,pyb.Pin.OUT_PP)
        self.pinA15.low()  #possibly not needed
        
        try:
            ## nFAULT pin object (high when ordinary, low when fault occurs)
            self.pinB2 = pyb.ExtInt(nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP,  self.faultBack)
        except ValueError:
            print('External Interrupt Already set up')
        
        ## Motor IN1 pin object
        self.pinB4 =pyb.Pin(IN1_pin)
        
        ## Motor IN2 pin object
        self.pinB5 =pyb.Pin(IN2_pin)
        
        ## Timer pin object 
        self.tim3 = pyb.Timer(timer,freq = 20000)
        
        #determine which motor to set up
        if IN1_pin == 'B4':
            ## First motor input pin timer channel object
            self.t3ch1 = self.tim3.channel(1,pyb.Timer.PWM, pin = self.pinB4)
            
            ## Second motor input pin timer channel object
            self.t3ch2 = self.tim3.channel(2,pyb.Timer.PWM, pin = self.pinB5)
        elif IN1_pin == 'B0':
            self.t3ch1 = self.tim3.channel(3,pyb.Timer.PWM, pin = self.pinB4)
            self.t3ch2 = self.tim3.channel(4,pyb.Timer.PWM, pin = self.pinB5)
            
        #create state variable for use as nFAULT trigger
        lab0x03share.Motor_state = 0
        print('Initialized Motor Driver Pins - Motor Driver Class')
        
        
        
        
    def enable (self):
        '''
        @brief      This method enables the pin which allows the motor to run.
        @details    When using this method make sure to disable the nFAULT pin before pinA15 is set to high, then re-Enable the nFAULT pin
        '''
        #print ('Enabling Motor')
        #print('before')
        #self.pinB2.disable()
        self.pinA15.high()
        #self.pinB2.enable()
        #print('after')
        #utime.sleep_ms(1000)
        
    def disable (self):
        '''
        @brief  This method disables the pin which allows the motor to run.
        '''
        #print ('Disabling Motor')
        self.pinA15.low()
        
    def set_duty (self, duty):
        ''' 
        @brief  This method sets the duty cycle.
        @details This method sets the duty cycle to be sent
                to the motor to the given level. Positive values
                cause effort in one direction, negative values
                in the opposite direction.
        @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor '''
            
        if duty > 0:
            self.t3ch2.pulse_width_percent(0)
            self.t3ch1.pulse_width_percent(duty)
            #rint('setduty pos')
        elif duty < 0 :
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(-duty) # the negative is needed because the pin cant send a negative voltage
            #print('set duty ng')
        elif duty == 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
            #print('set duty 0')

    def findFault(self):
        ''' 
        @brief  This method is used for testing annd returns the nFault pin value.
         '''
        return self.pinB2.value()
    
    def faultBack(self,_):
        ''' 
        @brief  This method is called when an external interrupt is triggered, it disables the motors.
        @details When the current limit is reached the external interrupt is triggerd and faultBack() calls the method disable() to stop the motor.
         '''
        self.disable()
        print('changed states')
        
        #REMEMBER TO DEBOUNCE by disabling and re-enabling pinB2 BEFORE ENABLING THE MOTOR AGAIN
        lab0x03share.Motor_state = 1  #filler state until we implement this motor driver with all the other hardware, This state is where the user input will be
        #self.enable()
        
        

#This is test code to see if the driver works correctly.            
if __name__ =='__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = 'A15';
    pin_nFAULT  = 'B2';
    pin_IN1     = 'B0';
    pin_IN2     = 'B1';
    
    
    pin_IN1_2     = 'B4';
    pin_IN2_2     = 'B5';
    
    # Create the timer object used for PWM generation
    tim     = 3;
    
    # Create a motor object passing in the pins and timer
    moe1     = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, tim)
    moe2     = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1_2, pin_IN2_2, tim)
    print(moe1.pinA15.value())
    
    
    #disableing and then enabling pinB2 doesnt cause a fault to not happen when pinA15 is set high()
    moe1.pinB2.disable()
    # Enable the motor driver
    moe1.enable()
    moe1.pinB2.enable()
    
    print(moe1.pinA15.value())
    count = 0
    for n in range (100000):
        count += 1
        #print(moe.findFault())
    # Set the duty cycle to 10 percent
        moe1.set_duty(-35)
        moe2.set_duty(-100)
        #print(moe.findFault())
        
        if count == 1000:
            
            print('Negative')
            print (n)
            count = 0
        
    
    moe1.disable()
    moe2.disable()
    '''for n in range(1000):
        moe.set_duty(0)
        print('Stopped')
        print(n)
    for n in range(10000):
        moe.set_duty(35)
        print('Positive')
        print (n)
    moe.disable()'''