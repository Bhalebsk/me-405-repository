## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is where I will be keeping all the assignments needed for ME405.
#
#  @section sec_lab ME 405 Labs
#  Below you will find the labs completed for ME405.
#
#  <U>Lab0x01 -
#  lab0x01.py</U>
#
#    This file simulates a vending machine. The user may press a number from 0-7 to add "money" to the vending machine.
#  Then the user my press p,d,s, or c, to select a "drink." The user may also press, e, to eject their "money." 
#
#  @image html hw0x00.jpg
#
#  <CENTER>Figure 1: State Diagram of a Vending Machine</CENTER>
#
#  The source code is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%201/lab0x01.py
#
#  <U>Lab0x02 - 
#  lab0x02.py</U>
#
#   This file serves as a reaction time test. The user runs this on a STM32L476 Nucleo, then the user presses the blue button
#  as soon as they see the green LED turn on. To end the reaction time test the user presses "ctrl+c." Then the average reaction time
#  is calculated and shown to the user.
#
#  The source code is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%202/lab0x02.py
#
#  <U>Lab0x03 - 
#  lab0x03main.py & lab0x03PCend.py</U>
#
#    These files work in conjunction in order to collect, transfer, process, and use data to create a plot and a .csv file.
#  A nucleo STM32L476 and a PC are required to run this program. The process is simple, rename the file lab0x03main.py to main.py and put the file on the nucleo, 
#  plug the nucleo into the PC using the port COM3. Then, press the blue button on the nucleo and then  press "g" on your PC keyboard after running the program lab0x03PCend.py on your PC.
#  Now a plot and a .csv file with the data collected will be created.
#
#  The source code for lab0x03main.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%203/lab0x03main.py
#
#  The source code for lab0x03PCend.py is located here:  https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%203/lab0x03PCend.py
#
#  <U>Lab0x04 - 
#  lab0x04mainAppend.py, mcp9808.py, MCUtemp.py & microCSV.py</U>
#
#  These files were designed for use together. lab0x04mainAppend.py is a finite state machine that uses the modules mcp9808.py, MCUtemp.py and microCSV.py
#  in order to collect ambient temperature data, MCU core temperature data and then store that data in a .csv file. The modules can be used as stand alone modules
#  and they do not need lab0x04mainAppend.py to run.
#
#  Below you will find data collected by lab0x04mainAppend.py, this data was collected in my room. It was interesting to see a dip in the temperature when I opened my door and left my room to go make lunch.
#
#  \htmlonly <style>div.image img[src="lab0x04data.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab0x04data.png
#
#  <CENTER>Figure 2: Temperature Data Collected from a Nucleo STM32L476 and mcp9808 Breakout Board</CENTER>
#
#  The source code for lab0x04mainAppend.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%204/lab0x04mainAppend.py
#
#  The source code for mcp9808.py is located here:  https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%204/mcp9808.py
#
#  The source code for MCUtemp.py is located here:  https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%204/MCUtemp.py
#
#  The source code for microCSV.py is located here:  https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%204/MCUtemp.py
#
#  <U>Lab0x05 and Lab0x06</U>
#
#  In order to see the work done for labs 5 and 6 please navigate to thier respective tabs on the left of your screen.
#
#  <U>Lab0x07 - 
#  Rtouch.py</U>
#
#  This file acts as a driver for a resistive touch panel which is connected to its related breakout board. It works by setting up various pins and reading an interger value which can be converted into a distance. Each axis, X, Y, and Z need to be scanned seperately. The code is scaled for the touch panel I am using so the touch panel outputs a distance in [m] instead of a scaled integer ADC value. 
#  This code can scan each axis once in approximately 1000us. Below you can find the results of the speed test with 1000 trials.
#  
#  
#  \code{.cmd}
#  [1142, 983, 984, 995, 996, 1015, 985, 989, 1001, 1028, 1030, 1001, 996, 996, 1011, 1005, 967, 1022, 1010, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 967, 965, 965, 966, 997, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 966, 966, 966, 967, 966, 967, 966, 967, 967, 967, 967, 967, 967, 965, 966, 966, 1082, 1142, 1098, 967, 967, 967, 967, 967, 967, 967, 967, 966, 970, 967, 967, 966, 966, 967, 966, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 968, 967, 967, 967, 967, 966, 966, 966, 967, 966, 967, 966, 967, 967, 967, 967, 967, 966, 966, 967, 966, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 966, 1313, 1223, 967, 967, 967, 967, 967, 967, 968, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 967, 966, 966, 965, 967, 967, 967, 967, 967, 968, 966, 967, 966, 967, 966, 967, 966, 967, 967, 967, 967, 967, 966, 968, 967, 967, 967, 967, 966, 3343, 988, 990, 997, 1003, 1005, 1009, 1005, 1001, 996, 1001, 1009, 1018, 1026, 1004, 999, 1012, 1002, 967, 967, 967, 966, 968, 967, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 966, 967, 966, 967, 966, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 968, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 966, 967, 967, 967, 967, 967, 967, 966, 966, 966, 966, 967, 966, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 966, 967, 966, 966, 966, 966, 966, 966, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 976, 967, 967, 967, 967, 965, 967, 967, 967, 967, 967, 967, 967, 966, 967, 966, 967, 967, 967, 967, 967, 967, 968, 966, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 965, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 967, 968, 3381, 981, 988, 991, 999, 997, 999, 992, 992, 994, 1000, 1011, 1001, 991, 1001, 985, 966, 967, 966, 966, 967, 966, 966, 967, 966, 967, 965, 966, 966, 967, 967, 966, 967, 967, 966, 966, 967, 966, 967, 966, 966, 967, 966, 966, 967, 966, 967, 967, 966, 966, 967, 966, 967, 967, 967, 966, 967, 967, 966, 966, 966, 967, 966, 967, 966, 967, 966, 967, 966, 966, 967, 966, 966, 967, 967, 967, 967, 966, 966, 967, 967, 966, 966, 967, 966, 966, 966, 967, 966, 967, 966, 966, 967, 966, 966, 966, 966, 966, 967, 966, 967, 966, 966, 966, 966, 967, 966, 966, 967, 966, 966, 967, 967, 966, 967, 966, 967, 966, 966, 967, 966, 966, 967, 966, 967, 967, 966, 965, 966, 967, 966, 984, 993, 966, 967, 967, 966, 967, 966, 967, 966, 967, 967, 966, 966, 967, 966, 965, 966, 967, 967, 1053, 1099, 1065, 966, 966, 967, 966, 967, 966, 966, 967, 966, 967, 966, 966, 967, 966, 967, 966, 967, 967, 966, 967, 966, 967, 966, 971, 3454, 981, 988, 994, 996, 999, 996, 989, 994, 1040, 1011, 1022, 997, 990, 994, 967, 968, 967, 967, 967, 969, 967, 967, 967, 967, 968, 967, 967, 968, 967, 967, 966, 967, 967, 967, 967, 966, 967, 967, 967, 966, 967, 967, 967, 968, 967, 967, 968, 967, 967, 967, 968, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 967, 966, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 968, 967, 968, 967, 968, 967, 967, 966, 966, 967, 967, 967, 966, 967, 967, 967, 966, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 968, 967, 967, 968, 967, 967, 965, 965, 967, 967, 966, 967, 967, 967, 966, 967, 966, 967, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 1017, 1040, 1028, 967, 967, 966, 968, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 968, 968, 970, 3446, 981, 987, 994, 996, 999, 996, 989, 993, 995, 1010, 1023, 997, 990, 994, 966, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 966, 967, 967, 968, 967, 968, 967, 967, 968, 967, 967, 968, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 966, 967, 967, 967, 967, 966, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 966, 966, 966, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 966, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 966, 967, 966, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 968, 967, 967, 967, 968, 966, 967, 967, 967, 1017, 1040, 1029, 968, 967, 967, 968, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 965, 967, 968, 970, 3436, 981, 987, 993, 996, 999, 996, 989, 993, 995, 1010, 1023, 997, 991, 993, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 967, 967, 967, 967, 966, 968, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 965, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 966, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 967, 968, 967, 968, 967, 968, 967, 967, 966, 965]
#  983.962  avg test time in us - 1000 trials
#  \endcode
#  
#  Below you can see various figures outlining the scaling process. I apologize for the poor quality pictures, it was difficult to balance the rotating platform with one hand and get a clear picture with my other hand.
#
#  \htmlonly <style>div.image img[src="boundary.jpeg"]{width:1000px;}</style> \endhtmlonly 
#  @image html boundary.jpeg
#
#  <CENTER>Figure 3: Resistive Touch Screen Boundary Outline, the post its that are in-line with eachother show the boundaries for the X and Y direction.</CENTER>
#
#  \htmlonly <style>div.image img[src="X_dist1.jpeg"]{width:1000px;}</style> \endhtmlonly 
#  @image html X_dist1.jpeg
#
#  <CENTER>Figure 4: X Distance part 1</CENTER>
#
#  \htmlonly <style>div.image img[src="X_dist2.jpeg"]{width:1000px;}</style> \endhtmlonly 
#  @image html X_dist2.jpeg
#
#  <CENTER>Figure 5: X Distance part 2</CENTER>
#
#  Figures 4 and 5 show the total readable length in the x-direction.
#
#  \htmlonly <style>div.image img[src="Y_dist.jpeg"]{width:1000px;}</style> \endhtmlonly 
#  @image html Y_dist.jpeg
#
#  <CENTER>Figure 6: This figure shows the total readable distance in the y-direction.</CENTER>
#
#  The source code is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%207/Rtouch.py
#
#  <U>Lab0x08 - 
#  MotorDriver.py, MotorEncoder.py, lab0x03share.py</U>
#
#  The purpose of this lab was to design a motor driver and motor encoder. These two drivers will work together to determine the position of the motor and change to position of the motor.
#  The bulk of the MotorDriver.py code was written during the lab for ME305.
#  MotorDriver.py works by controlling the speed of the motor with PWM. MotorEncoder.py works by using a timer which counts in ticks which are able to be converted to an angular position. lab0x03share.py may not be needed for this lab assignment, but will be useful when we need to combine various drivers for the term project.
#
#  The source code for MotorDriver.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%208/MotorDriver.py
#
#  The source code for MotorEncoder.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%208/MotorEncoder.py
#
#  The source code for lab0x03share.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%208/lab0x03share.py
#
##  <U>Lab0x09 - 
#  BalanceMain.py, MotorDriver.py, MotorEncoder.py, lab0x03share.py, Rtouch.py</U>
#
#  The purpose of this lab was to design a controller that would incorporate using PWM to control a motor, motor encoders to determine the angular position of the shaft, and a resistive touch panel that determines the position of the ball.
#  The control gains used for this lab were, [1, 0.07, 1.5, 0.9] in the form [x_dot, theta_dot, x, theta]. Please note that in order to run BalanceMain.py you must level the platform before you run the code. You can find the documentation for each file by clicking on their names above.
#  
#  Below you can see how the platform is set up.
#
#  \htmlonly <style>div.image img[src="Platform.jpg"]{width:1000px;}</style> \endhtmlonly 
#  @image html Platform.jpg
#
#  <CENTER>Figure 7: This figure shows Resistive touch panel along with the balancing platform. This platform rotates on two different axes. </CENTER>
#
#  \htmlonly <style>div.image img[src="LeverArm.jpg"]{width:1000px;}</style> \endhtmlonly 
#  @image html LeverArm.jpg
#
#  <CENTER>Figure 8: This figure shows the lever arm used to rotate the platform around one axis, there are two total motors, allowing the platform to be rotated around the 2 axis' mentioned above.</CENTER>
#
#  The source code for BalanceMain.py is located here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%208/BalanceMain.py
#
#  You find a video showing the platform in action here: https://www.youtube.com/watch?app=desktop&v=S8OiUb8cOJ8&feature=youtu.be
#
#
#
# \page page1 Lab0x05
#  \tableofcontents
#  Below you will find the derivation of the equations of motion for a ball moving on a rotating platform.
#  \section sec Equations of Motion
#  \htmlonly <style>div.image img[src="EOM1.jpg"]{width:1000px;}</style> \endhtmlonly 
#  @image html EOM1.jpg
#  \htmlonly <style>div.image img[src="EOM2.jpg"]{width:1000px;}</style> \endhtmlonly 
#  @image html EOM2.jpg
#  \htmlonly <style>div.image img[src="EOM3.jpg"]{width:1000px;}</style> \endhtmlonly 
#  @image html EOM3.jpg
#
#
# \page page2 Lab0x06
#  \tableofcontents
#  Below you will find the various simulations of the dynamic model for a platform which can rotate on two different axis' and will use a control system to balance a ball created in lab0x05. After attempting the derrivation for the dynamic model
#  multiple times, none of my models lined up with the model posted by Charlie, see below. Recall that <I>x</I> is the position of the ball and <I>thetay</I> is the angle of the platform.
#  \htmlonly <style>div.image img[src="lab0x05checkin.PNG"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab0x05checkin.PNG
#  <CENTER><b>Figure 1:</b> Charlie's Model </Center>
#  From this point forward the system dynamics will use Charlie's dynamic model.
#  \section sec System Simulation
#  The plots below were generate using the SciPy.signal module in python. The source code can be found here: https://bitbucket.org/Bhalebsk/me-405-repository/src/master/lab%206/lab0x06.py
#  <u><b>lab0x06 part 3a</b></u>
#  \htmlonly <style>div.image img[src="lab6_3a1.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3a1.png
#  \htmlonly <style>div.image img[src="lab6_3a2.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3a2.png 
#  \htmlonly <style>div.image img[src="lab6_3a3.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3a3.png
#  \htmlonly <style>div.image img[src="lab6_3a4.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3a4.png
#  <CENTER><b>Figures 2-5:</b> The intial conditions for this system were <I>x</I> = 0, <I>xdot</I> = 0, <I>thetay</I> = 0, <I>thetaydot</I> = 0. This system response may seem wrong. I believe it is correct due to the system model being linearized around <I>x</I> = 0 and <I>u</I> = 0. This means that as the analysis of the system moves further away from those conditions the model becomes increasingly inaccurate this can be seen in the plots above, the system 'thinks' that there is a platform for the ball to roll on forever, in reality this is not the case. Also, this is an open loop system so there is no correcting feedback given.</CENTER>
#  
#  <u><b>lab0x06 part 3b</b></u>
#  \htmlonly <style>div.image img[src="lab6_3b1.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3b1.png
#  \htmlonly <style>div.image img[src="lab6_3b2.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3b2.png 
#  \htmlonly <style>div.image img[src="lab6_3b3.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3b3.png
#  \htmlonly <style>div.image img[src="lab6_3b4.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3b4.png
#  <CENTER><b>Figures 6-9:</b> The intial conditions for this system were <I>x</I> = 0.05m, <I>xdot</I> = 0, <I>thetay</I> = 0, <I>thetaydot</I> = 0. Overall the respose of the system is very similar to part 3a which is to be expected. The intial position can be seen to be 0.05m in the ball position plot. Again, the large error in this simulation is due to the system being an open loop system with the system being lineraized around <I>x</I> = 0 and <I>u</I> = 0. </CENTER>
#  
#  <u><b>lab0x06 part 3c</b></u>
#  \htmlonly <style>div.image img[src="lab6_3c1.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3c1.png
#  \htmlonly <style>div.image img[src="lab6_3c2.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3c2.png 
#  \htmlonly <style>div.image img[src="lab6_3c3.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3c3.png
#  \htmlonly <style>div.image img[src="lab6_3c4.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3c4.png
#  <CENTER><b>Figures 10-13:</b> The intial conditions for this system were <I>x</I> = 0, <I>xdot</I> = 0, <I>thetay</I> = 5deg, <I>thetaydot</I> = 0. Overall the respose of the system is very similar to part 3a which is to be expected. The intial angular position of the platform can not be seen on the platform plot becuase 5 degrees is a very small number in radians. Again, the large error in this simulation is due to the system being an open loop system with the system being lineraized around <I>x</I> = 0 and <I>u</I> = 0. </CENTER>
#
#  <u><b>lab0x06 part 3d</b></u>
#  \htmlonly <style>div.image img[src="lab6_3d1.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3d1.png
#  \htmlonly <style>div.image img[src="lab6_3d2.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3d2.png 
#  \htmlonly <style>div.image img[src="lab6_3d3.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3d3.png
#  \htmlonly <style>div.image img[src="lab6_3d4.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_3d4.png
#  <CENTER><b>Figures 14-17:</b> The intial conditions for this system were <I>x</I> = 0, <I>xdot</I> = 0, <I>thetay</I> = 0, <I>thetaydot</I> = 0 and a motor impulse of 1 mNm*s. This system simulation is interesting, the shape of the <I>x</I> and <I>thetay</I> switch concavity this leads me to believe something is wrong in this simulation. Based on my intuition I believe that the lines shouldn't switch concavity, but should instead have a gradual change of slope all in the same concavity. Again, the large error in this simulation is due to the system being an open loop system with the system being lineraized around <I>x</I> = 0 and <I>u</I> = 0. </CENTER>
#   
#  <u><b>lab0x06 part 4</b></u>
#  \htmlonly <style>div.image img[src="lab6_4a1.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_4a1.png
#  \htmlonly <style>div.image img[src="lab6_4a2.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_4a2.png 
#  \htmlonly <style>div.image img[src="lab6_4a3.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_4a3.png
#  \htmlonly <style>div.image img[src="lab6_4a4.png"]{width:1000px;}</style> \endhtmlonly 
#  @image html lab6_4a4.png
#  <CENTER><b>Figures 18-21:</b> The intial conditions for this system were <I>x</I> = 0.05m, <I>xdot</I> = 0, <I>thetay</I> = 0, <I>thetaydot</I> = 0. This system simulation is different than the previous 4, this simulates a closed loop system which uses various gains to make the system stable. This system is also linearized around <I>x</I> = 0 and <I>u</I> = 0. It is odd that the system stabilizes when the ball gets to position <I>x</I> = 6m, this leads me to believe there is a unit error in the simulation or that the system stabilized the higher order terms and the position of the ball was a byproduct of stabilizing those higher order terms. On the angular velocity plot is may be useful to notice that the system makes one full cycle which causes a large overshoot. </CENTER>
# 
#
#  
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/20/2021
#