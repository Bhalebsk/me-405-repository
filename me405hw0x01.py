# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 09:46:51 2021

@author: Brand
"""



def getChange(price,payment):
    
    if payment >= price:
        
        #calculate delta
        delta = payment - price
        
        #print(delta)
        
        #calculate twenties
        twentiesHold = round((delta % 20),2)
        #print(twentiesHold)
        moneyInTwenties = delta - twentiesHold
        #print(moneyInTwenties)
        updateDelta = twentiesHold
        numberOfTwenties = int(moneyInTwenties/20)
        #print(updateDelta)
        
        #calculate tens
        tensHold = round((updateDelta % 10),2)
        moneyInTens = updateDelta - tensHold
        updateDelta = tensHold
        numberOfTens = int(moneyInTens/10)
        #print(updateDelta)
        
        #calculate fives
        fivesHold =round((updateDelta % 5),2)
        moneyInFives = updateDelta - fivesHold
        updateDelta = fivesHold
        numberOfFives = int(moneyInFives/5)
        #print(updateDelta)
        
        #calculate ones
        onesHold = round((updateDelta % 1),2)
        moneyInOnes = updateDelta - onesHold
        updateDelta = onesHold
        numberOfOnes = int(moneyInOnes/1)
        #print(updateDelta)
        
        #calculate quarters
        quartersHold = round((updateDelta % .25),2)
        moneyInQuarters = updateDelta - quartersHold
        #print(moneyInQuarters)
        updateDelta = quartersHold
        numberOfQuarters = int(moneyInQuarters/.25)
        #print(updateDelta)
        
        #calculate dimes
        dimesHold = round((updateDelta % .1),2)
        moneyInDimes = updateDelta - dimesHold
        #print(moneyInDimes)
        updateDelta = dimesHold
        numberOfDimes = int(moneyInDimes/.1)
        #print(updateDelta)
        
        #calculate nickels
        nickelsHold = round((updateDelta % .05),2)
        moneyInNickels = updateDelta - nickelsHold
        #print(moneyInNickels)
        updateDelta = nickelsHold
        numberOfNickels = int(moneyInNickels/.05)
        #print(updateDelta)
        
        #calculate pennies
        penniesHold = round((updateDelta % .01),2)
        moneyInPennies = updateDelta - penniesHold
        #print(moneyInPennies)
        updateDelta = penniesHold
        numberOfPennies = int(moneyInPennies/.01)
        #print(updateDelta)
        
        #payment tuple
        payment = (numberOfPennies, numberOfNickels, numberOfDimes, numberOfQuarters, numberOfOnes, numberOfFives, numberOfTens, numberOfTwenties)
        #print(payment)
        return payment
        #print('The correct change is, ', payment[7], ' twenty dollar bill(s), ', payment[6], ' ten dollar bill(s), ', payment[5], ' five dollar bill(s), ', payment[4], ' one dollar bill(s), ', payment[3], ' quarter(s), ', payment[2], ' dime(s), ', payment[1], ' nickel(s), and ', payment[0], ' pennies.')
    else:
        print('You did not pay enough. Try again!')     
    pass

if __name__ == "__main__":
    
    for n in range(1):
        getChange(19.01,40.00)
        getChange(12.34,777.77)
