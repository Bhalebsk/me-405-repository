# -*- coding: utf-8 -*-
## @file lab0x03main.py
#  lab0x03main.py acts as a data collection tool which sends the data collected to a PC. This requires the script lab0x03PCend.py.
#
#  The user may run this script with lab0x03PCend.py in order to produce a step response plot of the voltage of across a button when it is pushed.
#  This requires a Nucleo STM32L476. The file lab0x03PCend.py should be run on the PC. The user will run lab0x03main.py on the Nucleo and then push the blue button to collect the voltage data and then
#  the user should run lab0x03PCend.py and press the letter g on their keyboard to start the data processing.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/30/2021
#
#  @package lab0x03main.py
#  lab0x03main.py acts as a data collection tool which sends the data collected to a PC. This requires the script lab0x03PCend.py.
#
#  The user may run this script with lab0x03PCend.py in order to produce a step response plot of the voltage of across a button when it is pushed.
#  This requires a Nucleo STM32L476. The file lab0x03PCend.py should be run on the PC. The user will run lab0x03main.py on the Nucleo and then push the blue button to collect the voltage data and then
#  the user should run lab0x03PCend.py and press the letter g on their keyboard to start the data processing.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/30/2021
"""
Created on Sat Jan 30 14:53:25 2021

@author: Brand
"""

import pyb
from pyb import UART

import utime

#set up comms
serComm = UART(2)

#set up data buffer for data collection
DataBufferSize = 2000
DataBuffer = [0]
timeDiff = [0]
BigList = []

#set up buttion for voltage reading
pinA0 = pyb.Pin.board.PA0
ButtonVoltage = pyb.ADC(pinA0)


#set up trigger for data collection
x= 0
dataCounter = 0

#visual for testing
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
    
def callback(_):
    '''
    Callback which runs when the user presses the blue button on a Nucleo STM32L476.
    The Callback saves the time when the button was pressed, turns the led off and sets a trigger(x=1).

    '''
    global x
    x=1
    
#set up button for interrupt
extintButton = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP,  callback)

#This loops is the main chunk of code. It waits for the user to press the blue button, then triggers an interrupt 
# which triggers the storing of ADC data. After the data is stored the program waits for
# a "g" or "G" to be sent via serial communication. The program then sends the stored ADC data back across to the computer which sent a "g" 
try:   
    while True:
        
        #This if sends data
        if x == 0:
            if serComm.any():
                #store value for next if statement
                val = serComm.readchar()
                if val == 71 or val == 103: #71 = G, 103 = g
                    #send data to PC
                    serComm.write(str(BigList))
                    
            continue
        
        #this if waits for a button press and then stores data, LED is for visual feedback
        elif x == 1:
            blinko.high()
            startTime = utime.ticks_us()
            DataBuffer.append( ButtonVoltage.read())
            endTime = utime.ticks_us()
            timeDiff.append(utime.ticks_diff(endTime,startTime))
            dataCounter +=1
            if dataCounter >= DataBufferSize:
                blinko.low()
                BigList.append(DataBuffer)
                BigList.append(-7777)
                BigList.append(timeDiff)
                x = 0
            else:
                pass
            
#if there is a keyboard interrupt get rid of external interrupt callback            
except KeyboardInterrupt:
    extintButton = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP,  callback = None)
    print('KeyBoard interrupt received')

