# -*- coding: utf-8 -*-
## @file lab0x03PCend.py
#  lab0x03PCend.py acts as a communication terminal with the nucleo. This requires the script lab0x03main.py.
#
#  The user may run this script in order to produce a step response plot of the voltage of across a button when it is pushed.
#  This requires a Nucleo STM32L476. The file lab0x03main.py should be run on the nucleo, the user will push the blue button to collect the voltage data and then
#  the user should run lab0x03PCend.py and press the letter g on their keyboard to start the data processing.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/30/2021
#
#  @package lab0x03PCend.py
#  lab0x03PCend.py acts as a communication terminal with the nucleo. This requires the script lab0x03main.py.
#
#  The user may run this script in order to produce a step response plot of the voltage of across a button when it is pushed.
#  This requires a Nucleo STM32L476. The file lab0x03main.py should be run on the nucleo, the user will push the blue button to collect the voltage data and then
#  the user should run lab0x03PCend.py and press the letter g on their keyboard to start the data processing.
#  This program is not to be run multiple times without restarting the program, that is to say it will collect data one time and the rewrite the old data.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 1/30/2021
"""
Created on Thu Jan 28 09:54:35 2021

@author: Brand
"""

import serial
import keyboard
import matplotlib.pyplot as plt
import csv


#set up communication with Nucleo
serialPC= serial.Serial(port = 'COM3', baudrate = 115273, timeout =1)
## Sends a character to the nucleo then reads a string from the nucleo
#
#  sendChar() writes the character sent from the users keyboard to the nucleo. sendChar() then reads a string from the nucleo and saves it as myval.
def sendChar():
    #sets up global variables
    global pushed_key,myval
    
    serialPC.write(str(pushed_key).encode('ascii'))
    myval = serialPC.readline().decode('ascii')
    return myval

#sets up variables for use later
pushed_key = None
myval = None



def on_keypress (thing):
    """ Callback which runs when the user presses a key. on_keypress saves the key pushed as a variable, pushed_key. This function was given to the class by the instructor.
    """
    #set up globals for use later
    global pushed_key

    pushed_key = thing.name
   

keyboard.on_press(on_keypress)

#set up lists to append to later
StepResponse= [0]
TimeStepResponse = [0]
NewTimeUS = []
NewVoltageList = []

#set up data sorting triggers
StartTrigger = 1
EndTrigger = 1
EndFound = 0
startGraph = 0
TotalTime = 0
k= 0

try:
    while True:
        #This is where the letter g gets sent to the nucleo and it is where the PC receive the voltage data.
        if pushed_key ==  'g' or pushed_key == 'G':
            
            #run sendChar() function
            print(sendChar())
            
            #this 'resets' pushed_key with a letter that will not be used in this program
            pushed_key = 'z'
            
            #after sendChar() runs and pushed key is reset start next process by switching this trigger to 1
            startGraph = 1
            
            #This is where the formatting happens
        elif startGraph == 1:
            
            #the data gets sent as one string the first step is to make the data into two lists
            myvalList = myval.split('-7777')
            
            #Here the two sets of data are seperated and formatted
            VoltageList = myvalList[0]
            VoltageList =  VoltageList.strip('[]').split(', ')
            TimeUS = myvalList[1]
            TimeUS =  TimeUS.strip('[]').split(', ')
            
            #Since I used an unusual method of transfering data I have to remove the first 2 data points from the time data set and the last 2 data points from the voltage data set.
            #This also converts the strings to integers
            for i in range(len(TimeUS)):
                if i > 1:
                    NewTimeUS.append(int(TimeUS[i]))
            for i in range(len(VoltageList)):
                if i < 2000:
                    NewVoltageList.append(int(VoltageList[i]))
            
            
            #The data was sent as 2000 data points, the actual step response is only 10-20 data points
            #below the code sorts and finds where the step response is and creates 2 new lists
            for i in range(len(NewVoltageList)):
                
                #read through every variable
                checkVal = NewVoltageList[i]
                
                #run until start position is found
                if StartTrigger == 1:
                    if checkVal > 100 and checkVal <4050:
                        StartPosition = i
                        StartTrigger = 0
                        
                #run until end position is found
                elif EndTrigger == 1:
                    if checkVal > 4050:
                        EndPosition = i
                        EndTrigger = 0
                        EndFound = 1
                        
                #once the end is found create the new sets of data, plot the data and save the data to a csv file.
                elif EndFound == 1:
                    
                    #this is the creation of the new data sets
                    for j in range(len(NewVoltageList)):
                        if j >= StartPosition - 1 and j <= EndPosition + 5:
                            
                            StepResponse.append(NewVoltageList[j])
                            
                            TimeStepResponse.append(NewTimeUS[j]+TimeStepResponse[k])
                            k +=1
                    
                    
                    #this creates the plot
                    plt.plot(TimeStepResponse,StepResponse)
                    plt.ylabel('Voltage Scale [ADC counts]')
                    plt.xlabel('Time [us]')
                    plt.title('Voltage over time')
                    #plt.show() does not seem to work the same way as shown in class
                    plt.show()
                    print('made fig')
                    
                    #This creates the csv file
                    with open('lab0x03.csv','w') as myfile:
                        writer = csv.writer(myfile)
                        writer.writerow(['Voltage ADC count','Time [us]'])
                        for i in zip(StepResponse,TimeStepResponse):
                            writer.writerow(i)
                    EndFound = 0
                    
            startGraph = 0
        else:
            pass

#This interrupt closes the program      and also closes the serial communication port.
except KeyboardInterrupt:  
    print('Ctrl+C was pressed')
    
    serialPC.close()
    