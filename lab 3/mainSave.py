# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 09:08:13 2021

@author: Brand
"""
import pyb
from pyb import UART
import array
#set up comms
serComm = UART(2)

#set up data buffer for data collection
DataBufferSize = 20
DataBuffer = array.array('H', (0 for index in range(DataBufferSize)))

#set up buttion for voltage reading
pinA0 = pyb.Pin.board.PA0
ButtonVoltage = pyb.ADC(pinA0)


#set up trigger for data collection
x= 0
dataCounter = 0

    
def callback(_):
    '''
    Callback which runs when the user presses the blue button on a Nucleo STM32L476.
    The Callback saves the time when the button was pressed, turns the led off and sets a trigger(x=1).

    '''
    global x
    x=1
    
#set up button for interrupt
extintButton = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP,  callback)
try:   
    while True:
        #ButtonVoltage = pyb.ADC(pinA0)
        if x == 0:
            if serComm.any():
                val = serComm.readchar()
                if val == 71 or val == 103: #71 = G, 103 = g
                    #serComm.write('do something')
                    
                    str(DataBuffer)
                    serComm.write(DataBuffer).endcode('ascii')
                    
                #serComm.write('you sent' + str(val) + 'to the nucleo')
            continue
        elif x == 1:
            DataBuffer[dataCounter] = ButtonVoltage.read()
            dataCounter +=1
            if dataCounter >= DataBufferSize:
                x = 0
            else:
                pass
except KeyboardInterrupt:
    extintButton = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP,  callback = None)
    print('KeyBoard interrupt received')

