# -*- coding: utf-8 -*-
"""
Created on Sat Feb 20 14:18:19 2021

@author: Brand
"""

from scipy import signal
import matplotlib.pyplot as plt
import sympy
from sympy import sin, cos
import math
import numpy as np

#Givens
r_m = 0.06      #[m]
l_r = 0.05      #[m]
r_b = 0.0105    #[m]
r_g = 0.042     #[m]
l_p = 0.11      #[m]
r_p = 0.0325    #[m]
r_c = 0.05      #[m]
m_b = 0.030     #[kg]
m_p = 0.400     #[kg]
I_p = 0.00188   #[kg*m^2]
b =  .010       #[N*m*s/rad]




#matrix math
#first set up symbols using sympy
r_m =  sympy.Symbol('r_m')
l_r =  sympy.Symbol('l_r')
r_b =  sympy.Symbol('r_b')
r_g =  sympy.Symbol('r_g')
l_p =  sympy.Symbol('l_p')
r_p =  sympy.Symbol('r_p')
r_c =  sympy.Symbol('r_c')
m_b =  sympy.Symbol('m_b')
m_p =  sympy.Symbol('m_p')
I_p =  sympy.Symbol('I_p')
b   =  sympy.Symbol('b')
g   =  sympy.Symbol('g')
T_x =  sympy.Symbol('T_x')


x_b =  sympy.Symbol('x_b')
xdot_b = sympy.Symbol('xdot_b')

theta = sympy.Symbol('theta')
theta_dot = sympy.Symbol('theta_dot')

#formula for moment of inertia for a ball
I_b = (2/5)*m_b*r_b


#set up equations to be put in M matrix, the equations below are from charlies derrivation
M11 = sympy.simplify(-(m_b*r_b**2 + m_b*r_c*r_b + I_b)/r_b)
M12 = sympy.simplify(-(I_b*r_b+I_p*r_b+m_b*r_b**3+m_b*r_b*r_c**2+2*m_b*r_b**2*r_c+m_p*r_b*r_c**2+m_b*r_b*x_b**2)/r_b)

M21 = sympy.simplify(-(m_b*r_b**2+I_b)/r_b)
M22 = sympy.simplify(-(m_b*r_b**3+m_b*r_c*r_b**2+I_b*r_b)/r_b)

M = sympy.Matrix([[M11,M12],[M21,M22]])

#set up equations to be put in F matrix, the equations below are from charlies derrivation
F1 = sympy.simplify(b*theta_dot - g*m_b*(sin(theta)*(r_b+r_c)+x_b*cos(theta))+T_x*l_p/r_m+2*m_b*theta_dot*x_b*xdot_b-g*m_p*r_g*sin(theta))
F2 = sympy.simplify(-m_b*r_b*x_b*theta_dot**2 - g*m_b*r_b*sin(theta))

F = sympy.Matrix([[F1],[F2]])
##Below follows the steps to put the Dynamic model into a space state equation and then linearlize that state space model around x = 0, u =0 
#invert M matrix

Minv = M.inv()

#multiply Minv with F, q_doubledot = Minv*F

q_doubledot = Minv*F

func_g = sympy.simplify(sympy.Matrix([[xdot_b],[theta_dot],[q_doubledot[0]],[q_doubledot[1]]]))


bigX = sympy.simplify(sympy.Matrix([[x_b],[theta],[xdot_b],[theta_dot]]))

Jx = sympy.zeros(4)
Ju = sympy.Matrix([[0],[0],[0],[0]])

for i in range (4):
    for j in range(4):
        Jx[i,j] = sympy.diff(func_g[i],bigX[j])
        
Jx = sympy.simplify(Jx)


for i in range(4):
    Ju[i] = sympy.diff(func_g[i],T_x)

A_ss = Jx.subs({x_b:0,T_x:0})
A_ss = sympy.simplify(A_ss)
B_ss = Ju.subs({x_b:0,T_x:0})
B_ss = sympy.simplify(B_ss)

##TRY CONVERTING THESE MATRICES TO A TRANSFER FUNCTION AND GRAPH THAT
#plug in variables
A_ss = A_ss.subs({r_m:0.06,l_r:0.05,r_b:0.0105,r_g:0.042,l_p:0.11,r_p:0.0325,r_c:0.05,m_b:0.03,m_p:0.4,I_p:0.00188,b:0.01,g:9.81})
B_ss = B_ss.subs({r_m:0.06,l_r:0.05,r_b:0.0105,r_g:0.042,l_p:0.11,r_p:0.0325,r_c:0.05,m_b:0.03,m_p:0.4,I_p:0.00188,b:0.01,g:9.81})

#Test code -> subs({r_m:0.06,l_r:0.05,r_b:0.0105,r_g:0.042,l_p:0.11,r_p:0.0325,r_c:0.05,m_b:0.03,m_p:0.4,I_p:0.00188,b:0.01,g:9.81})


#StateSpace

#C = [-0.05,-0.02,-0.3,-0.2]
C = [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]]
D = [[0],[0],[0],[0]]

'''A_ss = A_ss.subs({x_b:0,xdot_b:0,theta:5*(math.pi/180),theta_dot:0})
B_ss = B_ss.subs({x_b:0,xdot_b:0,theta:5*(math.pi/180),theta_dot:0})'''
save_A_ss = A_ss
save_B_ss = B_ss

##Linearize Equations around a zero point then save as np matrices not scipy data
A_ss = A_ss.subs({x_b:0,xdot_b:0,theta:0,theta_dot:0})
B_ss = B_ss.subs({x_b:0,xdot_b:0,theta:0,theta_dot:0})
A_ss = np.array(A_ss).astype(np.float64)
B_ss = np.array(B_ss).astype(np.float64)

isStable = np.linalg.eig(A_ss)   #check here for stability, if there is a posivtive number the system is unstable



length = 1
step = .01
TimeSteps = [0]
count = 0
for n in range(int(length/step)):
    count += step
    TimeSteps.append(count)

##set up initial conditions and run the system simulation to get data stored in variables t for time and y for a matrix with
# y[:,0]for position, y[:,1] for angle of the platform, y[:,2] for velocity, y[:,3] for angular velocity of the platform
# this runs step 3a from lab0x06 handout ME405

#X0 uses the same form as the state variable, X0 = [x_b,theta,xdot_b,theta_dot]
X0 = [0,0,0,0]
system = signal.StateSpace(A_ss,B_ss,C,D)
t,y = signal.step(system,X0 = X0,T = TimeSteps)


plt.figure(1)
plt.plot(t,y[:,0])
plt.ylabel('X - position [m]')
plt.xlabel('Time [s]')
plt.title('Distance Traveled - Open Loop Case a')

plt.figure(2)
plt.plot(t,y[:,1])  
plt.ylabel('Theta [rad]')
plt.xlabel('Time [s]')
plt.title('Angle of the Platform - Open Loop Case a')

plt.figure(3)
plt.plot(t,y[:,2])
plt.ylabel('Xdot - Velocity [m/s]')
plt.xlabel('Time [s]')
plt.title('Velocity of the Ball - Open Loop Case a')

plt.figure(4)
plt.plot(t,y[:,3])
plt.ylabel('Theta dot [rad/s]')
plt.xlabel('Time [s]')
plt.title('Angular Velocity of the Platform - Open Loop Case a')



# this runs step 3b from lab0x06 handout ME405
length = .4
step = .01
TimeSteps = [0]
count = 0
for n in range(int(length/step)):
    count += step
    TimeSteps.append(count)

X0b = [.05,0,0,0]
tb,yb = signal.step(system,X0 = X0b,T = TimeSteps)


plt.figure(9)
plt.plot(tb,yb[:,0])
plt.ylabel('X - position [m]')
plt.xlabel('Time [s]')
plt.title('Distance Traveled - Open Loop Case b')

plt.figure(10)
plt.plot(tb,yb[:,1])  
plt.ylabel('Theta [rad]')
plt.xlabel('Time [s]')
plt.title('Angle of the Platform - Open Loop Case b')

plt.figure(11)
plt.plot(tb,yb[:,2])
plt.ylabel('Xdot - Velocity [m/s]')
plt.xlabel('Time [s]')
plt.title('Velocity of the Ball - Open Loop Case b')

plt.figure(12)
plt.plot(tb,yb[:,3])
plt.ylabel('Theta dot [rad/s]')
plt.xlabel('Time [s]')
plt.title('Angular Velocity of the Platform - Open Loop Case b')


#this runs step 3c from lab0x06 handout ME405
X0c = [0,5*(math.pi/180),0,0]
system = signal.StateSpace(A_ss,B_ss,C,D)
tc,yc = signal.step(system,X0 = X0c,T = TimeSteps)


plt.figure(13)
plt.plot(tc,yc[:,0])
plt.ylabel('X - position [m]')
plt.xlabel('Time [s]')
plt.title('Distance Traveled - Open Loop Case c')

plt.figure(14)
plt.plot(tc,yc[:,1])  
plt.ylabel('Theta [rad]')
plt.xlabel('Time [s]')
plt.title('Angle of the Platform - Open Loop Case c')

plt.figure(15)
plt.plot(tc,yc[:,2])
plt.ylabel('Xdot - Velocity [m/s]')
plt.xlabel('Time [s]')
plt.title('Velocity of the Ball - Open Loop Case c')

plt.figure(16)
plt.plot(tc,yc[:,3])
plt.ylabel('Theta dot [rad/s]')
plt.xlabel('Time [s]')
plt.title('Angular Velocity of the Platform - Open Loop Case c')

#this runs step 3d from lab0x06 handout ME405
X0 = [0,0,0,0]
system = signal.StateSpace(A_ss,B_ss,C,D)
td,yd = signal.impulse(system,0.001,T = TimeSteps)


plt.figure(17)
plt.plot(td,yd[:,0])
plt.ylabel('X - position [m]')
plt.xlabel('Time [s]')
plt.title('Distance Traveled - Open Loop Case d')

plt.figure(18)
plt.plot(td,yd[:,1])  
plt.ylabel('Theta [rad]')
plt.xlabel('Time [s]')
plt.title('Angle of the Platform - Open Loop Case d')

plt.figure(19)
plt.plot(td,yd[:,2])
plt.ylabel('Xdot - Velocity [m/s]')
plt.xlabel('Time [s]')
plt.title('Velocity of the Ball - Open Loop Case d')

plt.figure(20)
plt.plot(td,yd[:,3])
plt.ylabel('Theta dot [rad/s]')
plt.xlabel('Time [s]')
plt.title('Angular Velocity of the Platform - Open Loop Case d')



##Adding a controler and closing the loop
#

#set timer for 20s
length = 20
step = .01
TimeSteps = [0]
count = 0
for n in range(int(length/step)):
    count += step
    TimeSteps.append(count)
    
K = np.array([[-0.05], [-0.02], [-0.3], [-0.2]])

##solve for BK and new big-Xdot
#BK = np.matmul(B_ss,K)
B_ss_mod = np.transpose(B_ss)
BK = np.matmul(K,B_ss_mod)

XdotCL = A_ss - BK


#this runs step 4 from lab0x06 handout ME405
X0CL = [.05,0,0,0]
systemCL = signal.StateSpace(XdotCL,B_ss,C,D)
tCL,yCL = signal.step(systemCL,X0 = X0CL,T = TimeSteps)

plt.figure(5)
plt.plot(tCL,yCL[:,0])
plt.ylabel('X - position [m]')
plt.xlabel('Time [s]')
plt.title('Distance Traveled - Closed Loop')

plt.figure(6)
plt.plot(tCL,yCL[:,1])  
plt.ylabel('Theta [rad]')
plt.xlabel('Time [s]')
plt.title('Angle of the Platform - Closed Loop')

plt.figure(7)
plt.plot(tCL,yCL[:,2])
plt.ylabel('Xdot - Velocity [m/s]')
plt.xlabel('Time [s]')
plt.title('Velocity of the Ball - Closed Loop')

plt.figure(8)
plt.plot(tCL,yCL[:,3])
plt.ylabel('Theta dot [rad/s]')
plt.xlabel('Time [s]')
plt.title('Angular Velocity of the Platform - Closed Loop')