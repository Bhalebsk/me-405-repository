# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 13:35:28 2021

@author: Brand
"""

import pyb
import utime
## A Rtouch driver object
#
#  The Rtouch class is used with a resistive touch panel connected to a FPC (flexible printed circuitry) adapter which is connected to a nucleo stm32L476.
#  The Rtouch class reads the x position, y position and whether there is contact on the resistive touch panel.
#
#  @param XP                A string that corresponds to the X+ pin.
#  @param XM                A string that corresponds to the X- pin.
#  @param YP                A string that corresponds to the Y+ pin.
#  @param YM                A string that corresponds to the Y- pin.
#  @param width             An integer in [m] that corresponds to the X axis readable distance.
#  @param length            An integer in [m] that corresponds to the Y axis readable distance.
#  @param centerX           An integer that corresponds to the ADC value for the center of the X axis.
#  @param centerY           An integer that corresponds to the ADC value for the center of the Y axis.
#  @author Brandon Halebsky
#  @copyright License Info
#  @date 2/28/21
class Rtouch:
    ## Constructor for Resistive touch screen driver
    #
    #  The primary purpose of the constructor is to set up variables for cycling through various pin configurations in order to scan the X axis, Y axis and Z axis.
    #  The constructor also creates ratios for unit converions based on the user input.
    #  Note: you must pass each variable as a string ex. if XP correlated to ping A0 then XP = 'A0'
    def __init__(self,XP,XM,YP,YM,width,length,centerX,centerY):
        self.xp = XP
        self.xm = XM
        self.yp = YP
        self.ym = YM
        self.centerX = centerX
        self.centerY = centerY
        self.width_ratio = width/(3800-200)  #x distance longer side
        self.length_ratio = length/(3600-400) #y distance shorter side
        
    ## scanX() scans the X axis and returns a distance from the center of the resistive touch panel in [m].    
    #  IMPORTANT: in order to properly use scanX you must follow the order, scanX(), scanY(),scanZ() and you must run the sequence more than once
    #  this is because each method relys on the previous state of the port pins. This was done to optimize the speed of the scans.    
    def scanX(self):
        self.pin_yp = pyb.Pin(self.yp,pyb.Pin.IN)
        self.pin_xp = pyb.Pin(self.xp,pyb.Pin.OUT_PP).high()
        return (pyb.ADC(self.ym).read()-(self.centerX))*(self.width_ratio)
    
    ## scanY() scans the Y axis and returns a distance from the center of the resistive touch panel in [m].    
    #  IMPORTANT: in order to properly use scanY you must follow the order, scanX(), scanY(),scanZ() and you must run the sequence more than once
    #  this is because each method relys on the previous state of the port pins. This was done to optimize the speed of the scans. 
    def scanY(self):
        self.pin_xp = pyb.Pin(self.xp,pyb.Pin.IN)
        self.pin_ym = pyb.Pin(self.ym,pyb.Pin.OUT_PP).low()
        self.pin_yp = pyb.Pin(self.yp,pyb.Pin.OUT_PP).high()
        return (pyb.ADC(self.xm).read()-(self.centerY))*(self.length_ratio)
     
    ## scanZ() scans the entire resistive touch panel and determines wheter there is contact. scanZ() returns True if there is contact or False if there is no contact.  
    #  IMPORTANT: in order to properly use scanZ you must follow the order, scanX(), scanY(),scanZ() and you must run the sequence more than once
    #  this is because each method relys on the previous state of the port pins. This was done to optimize the speed of the scans. 
    def scanZ(self):
        self.pin_xm = pyb.Pin(self.xm,pyb.Pin.OUT_PP).low()
        if pyb.ADC(self.ym).read() >= 3900:
            return False
        else:
            return True
            
            
    def readAll(self):
        self.XYZ_data = (self.scanX(), self.scanY(), self.scanZ())
        return self.XYZ_data
        

if __name__ == "__main__":
    diff = []
    test = Rtouch('A7','A1','A6','A0',.179,.101,2000,1880)        
    for n in range(1000):
        startTime = utime.ticks_us()
        test.readAll()
        endTime = utime.ticks_us()
        diff.append( utime.ticks_diff(endTime,startTime))
    print(diff)
    x= 0
    for n in range(len(diff)):
        x += diff[n]
    print(x/len(diff), ' avg test time in us - 1000 trials')
    
    '''
    while True:
        print(test.readAll())
        utime.sleep_ms(1)'''
    '''
    testSave = []
    for n in range(7500):
        print(test.readAll())
        testSave.append(test.readAll())'''
        
        